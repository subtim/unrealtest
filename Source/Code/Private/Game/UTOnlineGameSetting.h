#pragma once

#include "OnlineSessionSettings.h"

class FUTOnlineSessionSettings : public FOnlineSessionSettings
{
public:

	FUTOnlineSessionSettings(bool bIsLAN = false, bool bIsPresence = false, int32 MaxNumPlayers = 4);
	virtual ~FUTOnlineSessionSettings() {}
};

class FUTOnlineSearchSettings : public FOnlineSessionSearch
{
public:
	FUTOnlineSearchSettings(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FUTOnlineSearchSettings() {}
};

class FUTOnlineSearchSettingsEmptyDedicated : public FUTOnlineSearchSettings
{
public:
	FUTOnlineSearchSettingsEmptyDedicated(bool bSearchingLAN = false, bool bSearchingPresence = false);

	virtual ~FUTOnlineSearchSettingsEmptyDedicated() {}
};
