


#include "AbilitySystem/Actors/UTFireActor.h"
#include "GameFramework/ProjectileMovementComponent.h"

AUTFireActor::AUTFireActor()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(FName("ProjectileMovement"));
}

void AUTFireActor::BeginPlay()
{
	Super::BeginPlay();

}
