
#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "AbilitySystemComponent.h"
#include "UnrealTestAttributeSet.generated.h"

class UUnrealTestAbilitySystemComponent;
struct FGameplayEffectSpec;

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

DECLARE_MULTICAST_DELEGATE_FourParams(FUTAttributeEvent, AActor* /*EffectInstigator*/, AActor* /*EffectCauser*/, const FGameplayEffectSpec& /*EffectSpec*/, float /*EffectMagnitude*/);


UCLASS()
class UNREALTEST_API UUnrealTestAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
public:

	UUnrealTestAttributeSet();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
	virtual bool PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	virtual void PreAttributeBaseChange(const FGameplayAttribute& Attribute, float& NewValue) const override;
	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;

	mutable FUTAttributeEvent OnOutOfHealth;
	
	ATTRIBUTE_ACCESSORS(UUnrealTestAttributeSet, Health)
	ATTRIBUTE_ACCESSORS(UUnrealTestAttributeSet, MaxHealth)
	ATTRIBUTE_ACCESSORS(UUnrealTestAttributeSet, MoveSpeed)

	UWorld* GetWorld() const override;
	UUnrealTestAbilitySystemComponent* GetUTAbilitySystemComponent() const;

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_Health)
	FGameplayAttributeData Health;

	UPROPERTY(BlueprintReadOnly, Category = "Health", ReplicatedUsing = OnRep_MaxHealth)
	FGameplayAttributeData MaxHealth;

	UPROPERTY(BlueprintReadOnly, Category = "MoveSpeed", ReplicatedUsing = OnRep_MoveSpeed)
	FGameplayAttributeData MoveSpeed;

protected:
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute, const FGameplayAttributeData& MaxAttribute, 
									float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty);

	void ClampAttribute(const FGameplayAttribute& Attribute, float& NewValue) const;

	UFUNCTION() 
	virtual void OnRep_Health(const FGameplayAttributeData& OldHealth);
	UFUNCTION() 
	virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldMaxHealth);
	
	UFUNCTION() 
	virtual void OnRep_MoveSpeed(const FGameplayAttributeData& OldMoveSpeed);

private:
	mutable TWeakObjectPtr<UUnrealTestAbilitySystemComponent> AbilitySystemComponentPtr;
};
