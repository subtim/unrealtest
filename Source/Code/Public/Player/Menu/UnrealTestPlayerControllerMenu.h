
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "UnrealTestPlayerControllerMenu.generated.h"

class AUnrealTestGameSession;

UCLASS()
class UNREALTEST_API AUnrealTestPlayerControllerMenu : public APlayerController
{
	GENERATED_BODY()

protected:
	virtual void Tick(float DeltaSeconds) override;

	AUnrealTestGameSession* GetGameSession() const;
	
};
