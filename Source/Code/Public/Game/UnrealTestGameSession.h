
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameSession.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "UnrealTestGameSession.generated.h"

struct FUTGameSessionParams
{
	FName SessionName = NAME_None;
	bool bIsLAN = false;
	bool bIsPresence = false;
	TSharedPtr<const FUniqueNetId> UserId;
	int32 BestSessionIdx = 0;

	FUTGameSessionParams() = default;
};

UCLASS()
class UNREALTEST_API AUnrealTestGameSession : public AGameSession
{
	GENERATED_BODY()
protected:
	AUnrealTestGameSession(const FObjectInitializer& ObjectInitializer);

	DECLARE_EVENT_TwoParams(AUnrealTestGameSession, FOnCreatePresenceSessionComplete, FName /*SessionName*/, bool /*bWasSuccessful*/);
	FOnCreatePresenceSessionComplete CreatePresenceSessionCompleteEvent;

	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;

	FOnCreatePresenceSessionComplete& OnCreatePresenceSessionComplete() { return CreatePresenceSessionCompleteEvent; }

	virtual void OnCreateSessionComplete(FName InSessionName, bool bWasSuccessful);

	virtual void RegisterServer() override;

	FUTGameSessionParams CurrentSessionParams;

	TSharedPtr<class FUTOnlineSessionSettings> HostSettings;
	
};
